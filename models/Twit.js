const Sequelize = require("sequelize");
const sequelize = require("../config/sequelize.js");

const Twit = sequelize.define(
  "twits",
  {
    name: Sequelize.STRING,
    user: Sequelize.STRING,
    date: Sequelize.DATE,
    image: Sequelize.INTEGER,
  },
  {
    timestamps: false
  }
);

module.exports = Twit;

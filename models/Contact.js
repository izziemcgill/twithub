const Sequelize = require("sequelize");
const sequelize = require("../config/sequelize.js");

const Contact = sequelize.define(
  "contacts",
  {
    user: Sequelize.STRING,
    last: Sequelize.DATE,
    contact: Sequelize.STRING,
    follows: Sequelize.INTEGER,
    followed: Sequelize.INTEGER,
    tweets: Sequelize.BIGINT,
    replies: Sequelize.BIGINT,
    retweets:  Sequelize.BIGINT,
    likes: Sequelize.BIGINT
  },
  {
    timestamps: false
  }
);

module.exports = Contact;

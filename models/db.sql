vacuum;

commit;
delete from tweets where user = 'micahflee';

select * 
  from tweets 
 where id 
    in (select id 
          from tweets
         group by id
         having count(*) > 1) 
 order by id

DROP TABLE "tweets"

CREATE TABLE "tweets" (
	id INTEGER PRIMARY KEY NOT NULL ,
	link TEXT(50),
	"user" TEXT(50) collate NOCASE,
	name TEXT(50) collate NOCASE,
	status TEXT(255),
	"date" TEXT(50),
	raw TEXT(2048),
	mood TEXT(2048),
	score NUMERIC,
	retweets NUMERIC,
	likes NUMERIC
);

INSERT OR REPLACE INTO tweets SELECT * FROM __tweets;


ALTER TABLE tweets ADD CONSTRAINT tweets_PK PRIMARY KEY (id) ;


 update tweets
		set status = replace(status, '${', '$\{')
-- select * 
   from tweets
  where status like '%${%'

update tweets
   set link = substr(raw, instr(raw, 'id_str":"')+9, instr(raw, '","text":')-(instr(raw, 'id_str":"')+9))
     , status = substr(raw, instr(raw, 'text":"')+7, instr(raw, '","truncated":')-(instr(raw, 'text":"')+7))
 where user = 'DoubleJake'


select *, substr(raw, instr(raw, 'text":"')+7, instr(raw, '","truncated":')-(instr(raw, 'text":"')+7))
  from tweets
 where id = '908436344279568400';

select raw
  from tweets
 where user = 'nthcolumn'
 order by id DESC
 
id = '908436344279568400';



select user
     , substr(
           raw
         , instr(raw, 'followers_count":')+17
         , instr(raw, ',"friends_count"')-(instr(raw, 'followers_count":')+17)
       )
     , strftime('%Y', date)
     , strftime('%m', date)
     , raw
     
     
select *
  from tweets
 where user = 'gossithedog'
 group by strftime('%Y', date), strftime('%m', date)
 order by strftime('%Y', date), strftime('%m', date)

update tweets
   set retweets = substr(raw, instr(raw, 'favorite_count":')+15, (instr(raw, ',"favorited"')-(instr(raw, 'favorite_count":')+15)))
 where retweets is NULL

update tweets
   set likes = substr(raw, instr(raw, 'followers_count":')+16, (instr(raw, ',"friends_count"')-(instr(raw, 'followers_count":')+16)))
 where likes is NULL

update tweets
   set status = 'dfssfdsfd'
   
 update tweets
    set status = replace(status, '${', '$\{')
  where status like '%${%'

  
 update tweets
    set status = replace(status, '\', '\ ')
  where status like '%\'
  
select * from tweets where status like '%\'
  
insert into tweets (id, link, user, name, status, date, raw, mood, score)
select id, link, user, name, status, date, raw, mood, score
  from __tweets

  
  delete from tweets where user = '0xerror' 

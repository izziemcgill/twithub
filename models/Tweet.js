const Sequelize = require("sequelize");
const { connection, Op } = require("../config/sequelize.js");

global.Buffer = global.Buffer || require('buffer').Buffer;

if (typeof btoa === 'undefined') {
  global.btoa = function (str) {
    return new Buffer(str, 'binary').toString('base64');
  };
}

if (typeof atob === 'undefined') {
  global.atob = function (b64Encoded) {
    return new Buffer(b64Encoded, 'base64').toString('binary');
  };
}

const Tweet = connection.define(
  "tweets",
  {
    id: {
      type: Sequelize.BIGINT,
      primaryKey: true
    },
    link: Sequelize.STRING,
    user: Sequelize.STRING,
    name: Sequelize.STRING,
    status: Sequelize.STRING,
    date: Sequelize.DATE,
    raw: Sequelize.STRING,
    mood: Sequelize.STRING,
    score: Sequelize.FLOAT,
    retweets: Sequelize.FLOAT,
    likes: Sequelize.FLOAT
  },
  {
    timestamps: false
  }
);

const deleteTweets = async (screen_name, since_id) => {
  return Tweet.destroy({
      where: { [Op.and]: [ { user: screen_name }, { id: { [Op.gt]: since_id } } ] }
  })
}

const getLatest = async (screen_name) => {

  // get latest tweets
  const Twitter = require('twitter');
  const config = require("../config/twitter");
  const client = new Twitter(config);
  const sentiment = require('sentiment');

  let params = {'screen_name': screen_name, count: 200 };
  let tweets = [], extra = [];

  var cutoffDate = new Date();
  cutoffDate.setMonth(cutoffDate.getMonth()-12)
  var lastTweetDate, lastUser ;

  try {

    do {
      extra = await client.get("statuses/user_timeline", params);
      tweets = tweets.concat(extra);
      max_id = params.max_id;
      if (tweets.length > 0){
        params.max_id = tweets[tweets.length - 1].id ;
        lastTweetDate = new Date(tweets[tweets.length-1].created_at);
        lastUser = tweets[tweets.length-1].user;
        console.log(`${tweets[tweets.length-1].created_at} :: ${lastTweetDate} :: ${params.max_id} ~ ${tweets.length}`);
      }
    } while (max_id != params.max_id && lastTweetDate > cutoffDate);

  } catch (error) {
    var message = JSON.stringify(error);
    console.log(`Twitter API error:: ${error} ${message}`);
  }

  //var deleted = await deleteTweets(screen_name, tweets[tweets.length-1].id);
  //console.log(deleted);

  // Add new tweets to cache
  try {

    records = tweets.map(tweet => {
      var mood = sentiment(tweet.text)
        , retweets = tweet.retweeted_status ? 0 : tweet.retweet_count
        , likes = tweet.favorite_count

      cleanText = tweet.text.replace('`', '');
      cleanText = tweet.text.replace(new RegExp('\`', 'g'), '\\\`');
      cleanText = tweet.text.replace(new RegExp('\\$', 'g'), '\\ ');

      if (tweet.id == 815737867179790300) {
        console.log(tweet.text.replace('\`', ''));
        console.log(`${tweet.id} \x1b[2J\x1b[2;2H\x1b[1m\x1b[35m \t ${ tweet.text } \t ${ retweets } \t ${ likes } \x1b[0m `);
      }

      return {
        id: tweet.id,
        link: tweet.id_str,
        user: tweet.user.screen_name,
        name: tweet.user.name,
        status: btoa(cleanText),
        date: tweet.created_at,
        raw: JSON.stringify(tweet),
        mood: JSON.stringify(mood),
        score: mood.score,
        retweets: retweets,
        likes: likes
      }
    });

    var options = { ignoreDuplicates: true };
    Tweet.bulkCreate(records, options);

  } catch (error) {
    var message = JSON.stringify(error);
    console.log(`Database error:: ${message}`)
    return `Database error:: ${message}`;
  }

  var data = {};
  data.user = lastUser;
  data.tweets = await Tweet.findAll({ where: { user: screen_name } });

  return data;
}

module.exports = { Tweet, getLatest };

// Include the server in your file
const server = require("server");
const { get, post } = server.router;
const { render, json } = server.reply;

const { getLatest } = require('../models/Tweet');
const { getUser, findUsers } = require('../lib/utils');

// PROFILE
const test = get("/test/:username", async ctx => {
  console.log(`Show profile for user ${ctx.params.username}`);
  let screen_name = ctx.params.username ;
  user = await getUser(screen_name);
  data = await getLatest(screen_name);
  Object.assign(data.user, user, data.user);
  return render("profile.html", { user: data.user, tweets: data.tweets });
});

module.exports = { test }

const Sequelize = require("sequelize");
const Op = Sequelize.Op;

const connection = new Sequelize('test.db', '', '', {
 storage: 'test.db',
 dialect: 'sqlite',
 operatorsAliases: Op, // use Sequelize.Op
 logging: false
});

module.exports = { connection, Op };

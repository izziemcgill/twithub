const request = require('request');
const cheerio = require('cheerio');

// Util functions to scrape twitter profile
const scrapeTwitter = username => {

  return new Promise((resolve, reject) => {

    var url = `https://twitter.com/${username}`;
    request.get(url, {}, (error, response, html) => {

      if (response.statusCode == 200 ) {
        //var $ = cheerio.load(html);

        avatar = html.match('data-url=(.*)[jpg|jpeg]');
        if (avatar) {
          avatar = avatar[0].replace('data-url="','');
        }
        // scrape user details including caps
        screen_name = html.match(/data-screen-name="(.*?)"/igm)[0].replace('data-screen-name="','').replace('"','');
        name = html.match(/data-name="(.*?)"/igm)[0].replace('data-name="','').replace('"','');
        counts = html.match(/data-count=[0-9]+/igm);
        try {
          tweeted = parseInt(counts[0].replace('data-count=',''));
          following = parseInt(counts[1].replace('data-count=',''));
          followers = parseInt(counts[2].replace('data-count=',''));
          likes = parseInt(counts[3].replace('data-count=',''));
        } catch(error) {
          console.log(error);
        }

        //$('.ProfileAvatar-image').each(function(i, element){
        //  console.log(`\x1b[2J\x1b[2;2H\x1b[1m\x1b[32m ${element.attribs.alt} \x1b[0m`);
        //  name = element.attribs.alt
        //})

        twit = { screen_name: screen_name, name: name, avatar: avatar, tweeted: tweeted, following: following, followers: followers, likes: likes };
        resolve(twit);

      } else {
        reject(error) ;
      }

    });

  });
}

const getUser = async (username) => {
  let user = await scrapeTwitter(username);
  return user;
}

const findUsers = async (search) => {

  return new Promise((resolve, reject) => {

    var url = `https://twitter.com/search?f=users&q=%40${search}&src=typd`;
    console.log(url);

    var data = [];
    request.get(url, {}, (error, response, html) => {
      if (response.statusCode == 200 ) {
        var $ = cheerio.load(html);
        $('.ProfileCard-avatarImage').each(function(i, element){
          console.log(`${i} ${element.attribs.src} ${element.parent.attribs.href} ${element.parent.attribs.title}`);
          twit = {
                   user: element.parent.attribs.href,
                   name: element.parent.attribs.title,
                   image: element.attribs.src
                 }
          data.push(twit);
          resolve(data);
        });
      } else {
        reject(error)
      }
    });
  });

}

module.exports = { getUser, findUsers };

// Include the server in your file
const server = require("server");
const { get, post } = server.router;
const { render, json } = server.reply;

const { Tweet, getLatest } = require('./models/Tweet');
const { getUser, findUsers } = require('./lib/utils');

const { test } = require('./controllers/test');

// SEARCH
const search = get("/search/:username", async ctx => {
  console.log(`Search for users matching ${ctx.params.username}`);
  items = await findUsers(ctx.params.username);
  if (items) {
    return json({ items: items });
  } else {
    return render("/error/404.html");
  }
});

// TIMELINE
const timeline = get("/timeline/:username", async ctx => {
  console.log(`Retrieve tweets for users matching ${ctx.params.username}`);
  let screen_name = ctx.params.username ;
  user = await getUser(screen_name);
  data = await getLatest(screen_name);
  Object.assign(data.user, user, data.user);
  if (data) {
    return json({ user: data.user, tweets: data.tweets });
  } else {
    return render("/error/404.html");
  }
});

// PROFILE
const profile = get("/profile/:username", async ctx => {
  console.log(`Show profile for user ${ctx.params.username}`);
  let screen_name = ctx.params.username ;
  user = await getUser(screen_name);
  if (!user.utc_offset) { user.utc_offset = 0};
  data = await getLatest(screen_name);
  Object.assign(data.user, user, data.user);
  return render("profile.html", { user: data.user, tweets: data.tweets });
});

const home = get("/", async ctx => { return render("index.html"); });
//const test = get("/test", async ctx => { return render("test.html"); });

// Handle requests to the url "/" ( http://localhost:3000/ )
console.log('\x1b[2J\x1b[2;2H\x1b[1m\x1b[31m Running on http://localhost:3000/. \x1b[0m');
server({ port: 3000 }, [ home, profile, search, test ]);
